package pl.sdacademy.springdemo.Movie;

import java.security.PrivateKey;

public class Movie {

    private int id;
    private String title;
    private String yearOfRelease;
    private String genre;

    public Movie(int id, String title, String yearOfRelease, String genre) {
        this.id = id;
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.genre = genre;
    }

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(String yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
