package pl.sdacademy.springdemo.Movie;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/movie")
@RestController
public class MovieController {

    List<Movie> movies = new ArrayList<>();

    @PostMapping("/add")
    public String addMovie(@RequestBody Movie movie) {
        movies.add(movie);
        return "Movie added!";
    }

    @GetMapping("/all")
    public List<Movie> listAllMovies(@RequestParam(required = false) String title) {
        if (title == null){
            return movies;
        }else{
            return movies.stream()
                    .filter(movie -> movie.getTitle().toUpperCase().contains(title.toUpperCase()))
                    .collect(Collectors.toList());
        }
    }
//    @DeleteMapping("/delete")
//    public String deleteMovie(@RequestParam int id){
//        movies.stream().
//                filter(movie -> {movie.getId() == id})
//                .
//        return "Movie deleted";
//    }
}
