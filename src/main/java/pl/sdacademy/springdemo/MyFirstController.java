package pl.sdacademy.springdemo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/hello")
@RestController
public class MyFirstController {

    @RequestMapping(method = RequestMethod.GET)
    public String helloWorld(){
        return "To ja DW!";
    }
}
