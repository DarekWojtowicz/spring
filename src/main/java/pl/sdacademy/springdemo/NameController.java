package pl.sdacademy.springdemo;

import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class NameController {
    User user = new User("Darek", "Wójtowicz");

    @GetMapping("/name")
    public String getName() {
        return user.getName();
    }

    @GetMapping("/lastName")
    public String getLastName() {
        return user.getLastName();
    }

    @GetMapping()
    public User getFullName() {
        return user;
    }

    @PostMapping(value = "/add")
    public String addUser(@RequestBody User user) {
        this.user=user;
        return "Zmieniono Name";
    }
}